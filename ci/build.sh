#!/bin/sh
set -e
set -u

###
### Main
###
case "$#" in

### Build file
1)  ### Variables
    BUILD_SCRIPT="$0"
    BUILD_SOURCE="${1}"
    BUILD_TARGET="$(dirname ${1})/$(basename ${1} .tmpl)"
    
    ### Notify user
    printf '+ %s -> %s\n' "${BUILD_SOURCE}" "${BUILD_TARGET}"

    ### Call build command
    envsubst <"${BUILD_SOURCE}" >"${BUILD_TARGET}" && rm -f "${BUILD_SOURCE}"
    ;;

### Main command
*)  ### Variables
    BUILD_SCRIPT="$0"

    ### Security check.
    if ! command -v envsubst
    then
        printf '! Command "envsubst" not found.\n'
        exit 1
    fi

    ### Notify user
    printf 'Building project\n'

    ### Action
    find . -type f -name '*.tmpl' -exec sh "${BUILD_SCRIPT}" {} \;
    ;;

esac
### 
### Completed
###