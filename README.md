# Phasme.org Core

## Warning.
This is a really low-level project. I'm not sure it will suits your needs.

## Name
You know [what it is](https://duckduckgo.com/?q=neohirasea).

## Install

Go to the [release page](https://gitlab.com/phasme.org/core/-/releases/permalink/latest), download [installer](https://gitlab.com/phasme.org/core/-/releases/permalink/latest/downloads/install.sh) and execute it.
```
# sh ./install.sh init my_project
```