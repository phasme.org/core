from jinja2 import Environment
from phasme.config import Config
from phasme.utils import _import_command
import logging
import pkgutil
import shutil
import sys
import textwrap

## Global objects
_logger = logging.getLogger(__name__)

def run(command:str=None, *args, **kvargs)->None:
    """
    Helpception !
    """

    ### Inspect packages
    if command:
        inspect_command(command)
    else:
        import command as pkg
        inspect_package(pkg)

###
### Display package help
###
def inspect_package(package):
    """
    """
    path = package.__path__
    name = package.__name__
    offset=8

    ###
    ### Global help
    ###
    print('Usage: stick.py <globals> <command> <arguments>')    
    print('Global options:')    
    print('--help, -h              Display <command> help. See hints.')
    print('--quiet, -q             Decrease verbosity')    
    print('--verbose, -v           Increase verbosity')    
    print()

    ###
    ### Commands list help
    ###
    print('Commands:')

    ### Loop through all packages
    for importer, modname, ispkg in pkgutil.walk_packages(path=path, prefix=name+'.', onerror=lambda x: None):
        print(f'  {modname[offset:]}')
    print()

    ###
    ### Hints
    ###
    print('Hint: Type "stick.py -h <command>" for more details about a specific command')

###
### DIsplay module help
###
def inspect_command(command):
    """
    """
    _config = Config()
    screen_w, screen_h = shutil.get_terminal_size()

    ### Load command module
    module = _inspect_load_module(command=command)

    ### Load help messages
    message = _inspect_load_help_message(command=command, module=module)

    ### Render help message
    _inspect_render_message(command=command, message=message, banner=screen_w, config=_config)

def _inspect_load_module(command):
    try:
        _logger.debug(f'Loading command "{command}"')
        return _import_command(command=command)
    except ModuleNotFoundError:
        if _logger.level <= logging.DEBUG:
            _logger.exception(f'Command "{command}" not found')
        else:
            _logger.error(f'Command "{command}" not found')
        sys.exit(1)

def _inspect_load_help_message(command, module):
    _message = None

    ### Load message from run() function
    if hasattr(module, 'run'):
        _message = module.run.__doc__

    ### Nothing to say ?
    if not isinstance(_message, str):
        _message = f'Command "{command}" has nothing to say.'
        
    ### Done
    return _message

def _inspect_render_message(command, message, *, config, banner=None):

    ### Display generic message
    if isinstance(banner, int):
        print('')
        print('┏','━' * (banner-2), '┓', sep='')
        print('┃', f'{{:^{banner-2}}}'.format(command), '┃', sep='')
        print('┗','━' * (banner-2), '┛', sep='')
        print('')
    
    ### Display usage
    print(f'Usage: stick.py {command} <options>')

    ### Use jinja2 engine
    engine = Environment()

    ### Load help message template
    template = engine.from_string(textwrap.dedent( message ))

    ### Generate data
    print( template.render(**config.all()) )
