from pathlib import Path
from phasme.config import Config
from phasme.utils import toSnakeCase, call, _config_load_files
import logging
import logging.config
import os

class Phasme:

    ###
    ### Init phase
    ###

    def init(self, root) -> None:
        """
        """
        ### Init config object
        self.__init_config(root=root)

        ### Init Logger object
        self.__init_logger()
        
        ### Notify user
        self.__logger.info('Phasme is starting')

    def __init_config(self, root) -> None:
        """
        Initialise config object
        """
        self.__config = Config()

        ### Set static values
        self.__config.set('data', {})
        self.__config.set('env', {})
        self.__config.set('lang', {})
        self.__config.set('path', {'root': root})

        ### Load system environent
        for key, value in os.environ.items():
            if key.startswith('PHASME_'):
                self.__config.env[ key[7:].upper() ] = value
        
        ### Load environment files
        _config_load_files(base=root, pattern='env.yml')
        _config_load_files(base=root, pattern=f'env.{self.__config.environment()}.yml')

        ### Init Path
        self.__init_path()

        ### Load config files
        if 'config' in self.__config['path']:
            _config_load_files(base=self.__config.path('config'), pattern='*.yml')
            _config_load_files(base=self.__config.path('config'), pattern=f'{self.__config.environment()}/*.yml')

        ### Load data files
        if 'data' in self.__config['path']:
            _config_load_files(base=self.__config.path('data'), pattern='*.yml', context='data')

    def __init_path(self) -> None:
        """
        """
        for key, path in self.__config.get('path').items():
            path = Path(path)

            ### Path is absolute.
            if not path.is_absolute():
                path = self.__config.path() / path

            ### Save path 
            self.__config['path'][key] = path
            
    def __init_logger(self) -> None:
        """
        Initialise logger object
        """

        ### Load logger config
        if 'logger' in self.__config:
            logging.config.dictConfig(self.__config['logger'])

        ### Build logger
        self.__logger = logging.getLogger(__name__)

    ###
    ### Start phase
    ###

    def start(self, __arguments) -> None:
        """
        """

        ### Parsecommand line arguments  
        _global, _commands = self._parse_command_line_arguments(__arguments)      

        ### Load global attributes
        self._parse_global_arguments(_global)
        self._apply_global_arguments(self.__config['global'], _commands=_commands)

        ### No commands mean HELP
        if not _commands:
            _commands = [['help']]

        ### Call all commands
        for _command in _commands:
            self.__logger.info(f'🚀 {_command[0]}')
            call(*_command)

    def _parse_command_line_arguments(self, __arguments) -> tuple:
        """
        """
        self.__logger.debug('Processing command line arguments')
        
        ### Build global arguments
        _global = []
        while __arguments and __arguments[0][0] in ('-','+'):
            _global.append( __arguments.pop(0) )

        ### Build command arguments
        _commands = []
        while __arguments:
            _x = __arguments.pop(0)

            ### This is an new command
            if _x[0] not in ('-','+'):
                _commands.append([])
            
            ### Save command
            _commands[-1].append( _x )

        ### Completed
        return _global, _commands

    def _parse_global_arguments(self, __arguments) -> None:
        """
        Load global attributes from command line
        """
        _global = {}
        for _raw in __arguments:

            ### Short argument
            if '=' in _raw:
                key, value = _raw.split('=', 1)
                key = toSnakeCase(key)
            
            ### Long argument
            else:
                key, value = toSnakeCase(_raw), True
            
            ### Set argument
            if key in _global:
                self.__logger.debug(f'+ "{key}" = {value}')  
                if isinstance(_global[key], list):
                    _global[key].append(value)
                else:
                    _global[key] = [ _global[key] , value ]
            else:
                self.__logger.debug(f'- "{key}" = {value}')
                _global[key] = value

        ### Save global arguments to config
        self.__config['global'] = _global

    def _apply_global_arguments(self, _globals, *, _commands) -> None:
        """
        """

        ### Update verbosity
        if 'v' in _globals or 'verbose' in _globals:
            self.__logger.debug('= Update verbosity to DEBUG level')
            logging.root.setLevel( logging.DEBUG )

        ### Update verbosity
        if 'q' in _globals or 'quiet' in _globals:
            self.__logger.debug('= Update verbosity to INFO level')
            logging.root.setLevel( logging.INFO )            

        ### Use inline help
        if 'h' in _globals or 'help' in _globals:
            self.__logger.debug('= Display inline help')

            ### Help all commands
            for _command in _commands:
                _command.insert(0, 'help')

            ### Add a default 'help' command
            if not _commands:
                _commands.append(['help'])
