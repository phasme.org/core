from typing import Any

###
### Main objects
###

class Config(object):
    __borg = {}
    __cache = {}

    def __new__(cls, *args, **kvargs):
        """
        We are one
        """
        self = object.__new__(cls)
        self.__dict__ = cls.__borg
        return self

    ###
    ### Haser
    ###

    def __contains__(self, __key:str) -> bool:
        return __key in self.__cache
    
    ###
    ### Getter
    ###

    def __getitem__(self, __key: str) -> Any:
        return self.__cache[__key]

    def get(self, __key: str, otherwise: Any = None) -> Any:
        return self.__cache.get(__key, otherwise)

    def all(self) -> dict:
        return self.__cache.copy()
    
    ###
    ### Setter
    ###

    def __setitem__(self, __key: str, __value: Any) -> None:
        self.__cache[__key] = __value

    def set(self, __key: str, __value: Any) -> None:
        self.__cache[__key] = __value

    def merge(self, __key: str, __value: Any) -> None:
        if __key in self.__cache:
            __temp = self.__cache[__key]
            __temp.append(__value)
            self.__cache[__key] = __temp
        else:
            self.__cache[__key] = [__value]

    ###
    ### Public shortcut
    ###
    
    def path(self, __key:str='root'):
        """
        """
        return self.__cache.get('path')[__key]
    
    def data(self, __key:str=None) -> Any:
        """
        """
        if __key is None:
            return self.__cache.get('data')
        else:
            return self.__cache.get('data')[__key]
    
    # get current environment
    def environment(self, otherwise:str='production') -> str:
        """
        Well, should NEVER fail neither ...
        """
        return self.get('app').get('env', otherwise)
    