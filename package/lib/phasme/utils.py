from collections import abc
from importlib import import_module
from pathlib import Path
import logging
import re

def toSnakeCase(string):
    return re.sub(r'(?<=[a-z])(?=[A-Z])|[^a-zA-Z]', '_', string).strip('_').lower()

###
### Command management
###
def _import_command(*, command, package='command'):
    return import_module(f'{package}.{command}')

def call(command, *args, **kvargs):
    """
    """
    _logger = logging.getLogger('phasme.core.call()')

    ### Notify user
    _logger.debug(f'🚗 {command}.run{args}{kvargs}')

    ### Load command module
    mod = _import_command(command=command)
    mod.run(*args, **kvargs)

###
### Config Helpers
###

def _config_load_files(base:Path, pattern:str, *, context:str=None, encoding=None) -> None:
    """
    """
    for file in base.glob(pattern):
        _config_load_file(file=file, context=context, encoding=encoding)

def _config_load_file(file:Path, *, context:str=None, encoding=None) -> None:
    """
    """
    from jinja2 import Environment
    from phasme.config import Config
    import yaml

    ### Get existing config
    config = Config()

    ### Create template environment
    # engine = Environment(loader=BaseLoader) - Not required anymore
    engine = Environment()

    ### Create template
    with open(file=file, mode='rt', encoding=encoding) as stream:
        template = engine.from_string( stream.read() )

    ### Generate data
    data = template.render(**config.all())

    ### Load data
    data = yaml.safe_load(data)

    ### Inject config data in place
    if context:
        _merge(config[context], data)
    else:
        _merge(config, data)

###
### Data helpers
###

def _merge(d, u):
    """
    Deep merge dict
    """
    for k, v in u.items():
        if isinstance(v, abc.Mapping):
            d[k] = _merge(d.get(k, {}), v)
        else:
            d[k] = v
    return d
