#!env python3
if __name__ == "__main__":

    ### Load dependencies
    from pathlib import Path
    import sys

    ### Update root path
    root = Path(sys.argv[0]).parent.absolute()
    sys.path.insert(0,str(root / 'lib'))

    ### Create app
    from phasme.core import Phasme
    app = Phasme()

    ### Init app
    app.init(root)

    ### Start app
    app.start(sys.argv[1:])